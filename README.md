
DIM2FHIR module
===============

This module translates personal health devices and measurements from
the IEEE 11073-20601 DIM into FHIR resources.



Documentation
-------------

This is an early prototype and not much documentation is available
at this point apart from inline comments in the source code.



Building
--------

The build system has not been fully completed yet, but it should
be possible to build the project on most platforms, although it
has only been tested on macOS.

You will need the baseplate headers to do this. CMake expects the
baseplate project to be a sibling to this project in the file system.
If this is not the case, you will need to manually edit the
`CMakeLists.txt` file

You will need CMake, GNU Make (or equivalent) and a recent C++
compiler.

Executing
```
cmake
make
```
should do the trick. 

To log debug information, add `-DDEBUG=ON` to the `cmake`
invocation.



Issue tracking
--------------

If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.



License
-------

The source files are released under Apache 2.0, you can obtain a
copy of the License at: http://www.apache.org/licenses/LICENSE-2.0
